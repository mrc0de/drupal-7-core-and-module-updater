'''
drupal-instance-backup.py - allows you to make a backup in zip form of a directory
It works by:

This script may be run in any directory, as both variables below are set at runtime.
	* path_to_backup - the path that you want to backup
	* path_to_backup_to - the path to backup your drupal installation to.

Note: 
	* final output includes a filename like: drupal_instance_backup_<<month>>-<<day>>-<<year>>--<<hour>>-<<minute>.tar.gz
	* Final output is compressed using gz-compression.
	* This takes a 15.2mb installation and compresses to 3.2mb (a near 80% percent reduction in size).

	@author Joseph Dickinson, 2015. 
'''

import os
import datetime
import tarfile

#Date creation and formatting.
date_time_format = "%b-%d-%Y--%H-%M"
date_time = datetime.datetime.today()
dt_final = date_time.strftime(date_time_format)
dt_final = str(dt_final)

#Path Info
path_to_backup = raw_input('Which directory should we backup ? \n')
path_to_backup_to = raw_input('Which directory would you like to backup to? \n')

print 'We will now backup to : ' + path_to_backup_to
print 'processing ...' 
try:
	with tarfile.open(path_to_backup_to+'drupal_instance_backup_'+dt_final+'.tar.gz', 'w:gz') as extractedtar:		
		extractedtar.add(path_to_backup, arcname=None, recursive=True)
except Exception as e:
	print e

print 'complete!! '

