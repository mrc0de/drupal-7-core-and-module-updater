# Drupal 7 *Core* & *Module* Updater 

#### Dependencies
######Python installed (2.7+) or greater on server
######Drupal 7 installation (note: can work with Drupal 6 or Drupal 8 with some modifications).

> **Drupal 7 Core and Module Updater** - A set of python scripts I developed to semi-automate and manage Drupal 7 installations. This repository specifically allows easy upgrading and updating of Drupal 7 Core and Modules.  It is invoked via a cli like Terminal on Linux.

============================
##UPDATING CORE
These set of files will allow making drupal [7] *core* updates increasingly easier to do...in few simple steps.
It can be used on both Drupal 7 core updates via CLI.

### How it works
*(Begin Automated processes)*
It works by recursively iterating through all Drupal-7 files (beginning at drupal root directory):
* It first `chmod the `.php` files to `777` namely: `'authorize.php'`, `'cron.php'`, `'index.php'`, `'install.php'`,`'update.php'`, `'xmlrpc.php'` as well as chmoding `web.conf` and old installation files (namely .txt).
- Then it removes these files found in the existing Drupal7 installation. 
- Next it locates the following directories: `'includes'`,`'misc'`,`'modules'`,`'scripts'`,`'themes'`.
- It then *recursively* traverses theses directories and `chmod`'s all the files to `777`.
- Then, it removes all of these directories.
- Then it prompts you to upload all of the new `.php` files and directories that were deleted.

*(Manual processes)*
- Connect via SFTP to your webserver
- Upload `drupal-core-update.py` to your Drupal 7 `root`.
   Once in root directory (with uploaded file), you may follow step 3.
- Next, upload the latest drupal version to your drupal root path.
- Finally, visit http://yoururl.com/drupalurlhere/update.php and follow the steps.

#### Finally, you've updated drupal core!

- To execute any *core file* updates run the script: `drupal-core-update.py` via:
```python
sudo python drupal-core-update.py
```
=============================
##UPDATING MODULES
### How it works
It works by recursively iterating through all Drupal-7 *directories* (beginning at your `module root` directory):

*(Begin Automated processes)*
- It first asks you what *modules* you'd like to delete.
- Then it asks if you have disabled the modules already.
- Upon answering 'Yes', it prompts you to see if you'd like to delete this module.
- If you type in 'Yes' again it will begin *recursively* removing the directory of the modules. 
  *Note: current bug in this process with deleting multiple modules at once.*
- Next it asks you to upload the latest module code.

*(Manual process)*
- Next to enable the modules again.
- Next, it reminds you to run `update.php` and turn off maintenance mode.

#### Finally, you've updated drupal module(s)!

To Run:
- To execute any *module* updates run the script: `drupal-module-update.py` via:
```python
sudo python drupal-module-update.py
```
Resulting example output:
- http://i.imgur.com/sGCFbEN.png (Core Updates)
- http://i.imgur.com/Arg1Cap.png (Module Updates)

=============================
##BACKING UP DRUPAL INSTALLATION
### How it works

You simply run the script by:
```python
sudo python drupal-instance-backup.py
```
It will prompt you for (2) things:
* Which directory do you want to backup?
* Where do you want to back this up?

*Note: * The file will use `gzip` compression, which reduces a plain drupal install files from 15.2mb to 3.2mb when compressed.

## Enjoy!
