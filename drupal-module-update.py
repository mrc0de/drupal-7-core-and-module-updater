#!/usr/bin/python
'''
	DUpdate - Drupal updater is a script used to semi-automate the process of
	updating a Drupal installation, specifically its modules.

	Note: Although drupal-core-update is built for Drupal7 : it can be used for a variety 
	of versions of Drupal (with some modifications).

	Built for Python 2.7.5

	@author Joseph Dickinson, 2015.
'''

import os
import shutil
from time import sleep


'''
	Specify files and directories to remove.
'''
files_to_delete = ['authorize.php', 'cron.php', 'index.php', 'install.php', 
				   'update.php', 'xmlrpc.php', 'web.conf','CHANGELOG.txt',
				   'COPYRIGHT.txt','INSTALL.mysql.txt','INSTALL.pgsql.txt',
				   'INSTALL.sqlite.txt','INSTALL.txt','LICENSE.txt',
				   'MAINTAINERS.txt','README.txt','UPGRADE.txt'
				   ]

dir_to_delete = ['includes','misc','modules','scripts','themes']

print 'Remember to first check:\n'
print 'The module update page, for the module you\'re updating. to see if deleting any of these files will hurt the upgrade process.'
print 'Put your site into maintenance mode.'
backupdb = raw_input('Backup your database and drupal files? [Yes] or [No]:\n')

try:
	if backupdb == 'Yes':
		to_delete_modules = raw_input('Which modules would you like to delete?\n')
		confirm_have_disabled_modules = raw_input('Have you disabled the following modules ? [Yes] or [No]:\n')
		print to_delete_modules.split(',')
		if confirm_have_disabled_modules == 'Yes':
			confirm_delete = raw_input('Do you want to remove the following modules now? [Yes] or [No]:\n')
			if confirm_delete == 'Yes':
				for module_to_delete in to_delete_modules.split(','):
					try:
						shutil.rmtree(os.getcwd() + '/' + module_to_delete)
						print 'removed module: ' + module_to_delete
					except:
						print 'There was an issue deleting module: ' + module_to_delete
				print '\n\n*********************************************\nNow please upload the latest module code \nand remember to activate your modules again.\n'
				print 'Next, run update.php: access http://yoursite.com/drupal/root/path/here/update.php'
				print 'Finally, turn off maintenance mode by visiting admin/settings/site-maintenance'
	elif backupdb != 'Yes':
		print '\n\n*****************************\nUh-oh, you need to backup your database and drupal files first.\nRemember to use Yes or No.'	
except:
	print 'An error occured, please contact author.'
