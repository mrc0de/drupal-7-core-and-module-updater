'''
	DUpdate - Drupal updater is a script used to semi-automate the process of
	updating a Drupal installation.

	Note: Although drupal-core-update is built for Drupal7 : it can be used for a variety 
	of versions of Drupal (with some modifications).

	Built for Python 2.7.5

	@author Joseph Dickinson, 2014.
'''

import os
import shutil
from time import sleep

'''
	Specify files and directories to remove.
'''
files_to_delete = ['authorize.php', 'cron.php', 'index.php', 'install.php', 
				   'update.php', 'xmlrpc.php', 'web.conf','CHANGELOG.txt',
				   'COPYRIGHT.txt','INSTALL.mysql.txt','INSTALL.pgsql.txt',
				   'INSTALL.sqlite.txt','INSTALL.txt','LICENSE.txt',
				   'MAINTAINERS.txt','README.txt','UPGRADE.txt'
				   ]

dir_to_delete = ['includes','misc','modules','scripts','themes']

'''
	For every file in files_to_delete, chmod the file to 777, then remove (due to permissions issues deleting otherwise).
		If the file isn't found, pass.
	For every directory in dir_to_delete, remove the whole tree; print removed directory.
		If there is an error removing a directory, mention which directory.
'''
for file in files_to_delete:
	try:
		os.chmod(file, 777)
		os.remove(file)
		print 'removed file: ' + file
	except:
		print 'file not found ' + file
		pass  #if it didn't remove the file for whatever reason, then just skip 

for dir in dir_to_delete:
	try:
		shutil.rmtree(os.getcwd() + '/' + dir)
		print 'removed directory: ' + dir
	except:
		print 'there is an error deleting directory ' + dir

print '******************************************\nPlease now ftp the new [core] drupal files:\n******************************************'
print 'upload directories: ' + 'includes | misc | modules | scripts | themes\n'
print 'upload files: ' + '|authorize.php|cron.php|index.php|install.php|update.php|xmlrpc.php|web.conf|CHANGELOG.txt|COPYRIGHT.txt|INSTALL.mysql.txt|INSTALL.pgsql.txt|INSTALL.sqlite.txt|INSTALL.txt|LICENSE.txt|MAINTAINERS.txt|README.txt|UPGRADE.txt|\n'
print 'Finally, make sure to visit <yourwebsitehere.com>/update.php after and follow prompts to complete installation.'
